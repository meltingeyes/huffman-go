package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"strconv"
)

func loadFreqTable(ft *map[string]int, source string) {
	b, err := ioutil.ReadFile(source)
	if err != nil {
		panic(err)
	}
	if err = json.Unmarshal(b, &ft); err != nil {
		panic(err)
	}
}

func main() {
	var freq map[string]int
	loadFreqTable(&freq, "data/freq.json")

	queue := buildPriorityQueue(freq)
	ht := buildHuffmanTree(queue)

	et := make(map[string]string, len(freq))
	buildEncodingTable(ht, et, "")
	for k, v := range et {
		if k == "\n" || k == " " {
			k = strconv.Quote(k)
		}
		fmt.Printf("%s\t%s\n", k, v)
	}
}
