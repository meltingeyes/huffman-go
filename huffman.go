package main

import (
	"container/heap"
)

type HuffTree struct {
	Leaf  *string
	Value int
	Left  *HuffTree
	Right *HuffTree
}

type PQ []*HuffTree

func (pq PQ) Len() int { return len(pq) }

func (pq PQ) Less(i, j int) bool {
	return pq[i].Value < pq[j].Value
}

func (pq PQ) Swap(i, j int) {
	pq[i], pq[j] = pq[j], pq[i]
}

func (pq *PQ) Push(x interface{}) {
	item := x.(*HuffTree)
	*pq = append(*pq, item)
}

func (pq *PQ) Pop() interface{} {
	old := *pq
	n := len(old)
	item := old[n-1]
	old[n-1] = nil
	*pq = old[0 : n-1]
	return item
}

func buildPriorityQueue(freqTable map[string]int) *PQ {
	pq := make(PQ, len(freqTable))
	i := 0
	for k, v := range freqTable {
		symbol := k // NB
		pq[i] = &HuffTree{
			Leaf:  &symbol,
			Value: v,
			Left:  nil,
			Right: nil,
		}
		i++
	}
	heap.Init(&pq)
	return &pq
}

func buildHuffmanTree(pq *PQ) *HuffTree {
	for pq.Len() > 1 {
		node1 := heap.Pop(pq).(*HuffTree)
		node2 := heap.Pop(pq).(*HuffTree)
		item := &HuffTree{
			Leaf:  nil,
			Value: node1.Value + node2.Value,
			Left:  node1,
			Right: node2,
		}
		heap.Push(pq, item)
	}
	return heap.Pop(pq).(*HuffTree)
}

func buildEncodingTable(ht *HuffTree, et map[string]string, path string) {
	if ht.Leaf != nil {
		et[*ht.Leaf] = path
		return
	}
	buildEncodingTable(ht.Left, et, path+"0")
	buildEncodingTable(ht.Right, et, path+"1")
}
