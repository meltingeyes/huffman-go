#!/usr/bin/env perl
use 5.026;
use warnings;

use JSON::PP ();

# calculates symbols' frequency in STDIN
# example usage:
# $ cat alice_in_wonderland.txt | ./freq_count.pl  >freq.json

my $freq_table;
while(<>) {
    my @symbols = split //;
    $freq_table->{$_}++ for (@symbols);
}

print JSON::PP->new->encode($freq_table);
